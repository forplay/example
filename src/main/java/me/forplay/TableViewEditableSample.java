package me.forplay;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * @author <a href="mailto:forhyz@gmail.com">Yuanzhi Hu</a>
 * @author HYZ create on 2018-07-14
 */

public class TableViewEditableSample extends Application {
    private TableView<Person> table = new TableView<Person>();
    private final ObservableList<Person> data =
            FXCollections.observableArrayList(
                    new Person("Jacob", "Smith", "jacob.smith@example.com"),
                    new Person("Isabella", "Johnson", "isabella.johnson@example.com"),
                    new Person("Ethan", "Williams", "ethan.williams@example.com"),
                    new Person("Emma", "Jones", "emma.jones@example.com"),
                    new Person("Michael", "Brown", "michael.brown@example.com"));
    final HBox hb = new HBox();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(new Group());
        stage.setTitle("Table View Sample");
        stage.setWidth(450);
        stage.setHeight(550);

        final Label label = new Label("Address Book");
        label.setFont(new Font("Arial", 20));

        table.setEditable(true);

        table.setRowFactory(
                new Callback<TableView<Person>, TableRow<Person>>() {
                    @Override
                    public TableRow<Person> call(TableView<Person> tableView) {
                        final TableRow<Person> row = new TableRow<>();
                        final ContextMenu rowMenu = new ContextMenu();
                        MenuItem editItem = new MenuItem("编辑");
                        editItem.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                System.out.println(row.getItem().firstName.get());
                                Person person = row.getItem();

                                String firstName = person.firstName.get();
                                String lastName = person.lastName.get();

                                Dialog dialog = new Dialog<Pair<String, String>>();
                                dialog.setTitle(firstName + "--" + lastName);
                                dialog.setHeaderText(null);

                                ButtonType confirmButtonType = new ButtonType("确定", ButtonBar.ButtonData.OK_DONE);
                                dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

                                GridPane grid = new GridPane();
                                grid.setHgap(10.0);
                                grid.setVgap(10.0);
                                grid.setPadding(new Insets(20.0, 150.0, 10.0, 10.0));

                                TextField tfFirstName = new TextField();
                                TextField tfLastName = new TextField();

                                tfFirstName.setText(firstName);
                                tfLastName.setText(lastName);

                                grid.add(new Label("fist name:"), 0, 0);
                                grid.add(tfFirstName, 1, 0);
                                grid.add(new Label("last name："), 0, 1);
                                grid.add(tfLastName, 1, 1);

                                Node confirmButton = dialog.getDialogPane().lookupButton(confirmButtonType);
                                tfFirstName.textProperty().addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        confirmButton.setDisable(newValue.trim().isEmpty());
                                    }
                                });

                                tfLastName.textProperty().addListener(new ChangeListener<String>() {
                                    @Override
                                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                                        confirmButton.setDisable(newValue.trim().isEmpty());
                                    }
                                });

                                dialog.getDialogPane().setContent(grid);
                                tfFirstName.requestFocus();

                                dialog.setResultConverter(new Callback<ButtonType, Pair<String, String>>() {
                                    @Override
                                    public Pair<String, String> call(ButtonType dialogButton) {
                                        if (dialogButton == confirmButtonType) {
                                            System.out.println("click yes!!!!");
                                            return new Pair<>(tfFirstName.getText(), tfLastName.getText());
                                        } else
                                            return null;
                                    }
                                });

                                Optional<Pair<String, String>> result = dialog.showAndWait();

                                result.ifPresent(new Consumer<Pair<String, String>>() {
                                    @Override
                                    public void accept(Pair<String, String> person) {
                                        System.out.println("first name=" + person.getKey() + ", last name=" + person.getValue());
                                    }
                                });
                            }
                        });
                        MenuItem removeItem = new MenuItem("删除");
                        removeItem.setOnAction(new EventHandler<ActionEvent>() {

                            @Override
                            public void handle(ActionEvent event) {
                                table.getItems().remove(row.getItem());
                            }
                        });
                        rowMenu.getItems().addAll(editItem, removeItem);

                        row.contextMenuProperty().bind(
                                Bindings.when(Bindings.isNotNull(row.itemProperty()))
                                        .then(rowMenu)
                                        .otherwise((ContextMenu) null));
                        return row;
                    }
                });


        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setMinWidth(100);
        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Person, String>("firstName"));
        firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Person, String>>() {
                    @Override
                    public void handle(CellEditEvent<Person, String> t) {
                        ((Person) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setFirstName(t.getNewValue());
                    }
                }
        );

        TableColumn lastNameCol = new TableColumn("Last Name");
        lastNameCol.setMinWidth(100);
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<Person, String>("lastName"));
        lastNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        lastNameCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Person, String>>() {
                    @Override
                    public void handle(CellEditEvent<Person, String> t) {
                        ((Person) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setLastName(t.getNewValue());
                    }
                }
        );

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(200);
        emailCol.setCellValueFactory(
                new PropertyValueFactory<Person, String>("email"));
        emailCol.setCellFactory(TextFieldTableCell.forTableColumn());
        emailCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Person, String>>() {
                    @Override
                    public void handle(CellEditEvent<Person, String> t) {
                        ((Person) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setEmail(t.getNewValue());
                    }
                }
        );

        emailCol.setCellFactory(
                new Callback<TableColumn<Person, String>,
                        TableCell<Person, String>>() {
                    @Override
                    public TableCell<Person, String> call(
                            TableColumn<Person, String> col) {

                        final TableCell<Person, String> cell = new TableCell<>();
                        cell.textProperty().bind(cell.itemProperty());
                        cell.itemProperty().addListener(new ChangeListener<String>() {
                            @Override
                            public void changed(ObservableValue<? extends String> obs,
                                                String oldValue, String newValue) {
                                if (newValue != null) {
                                    final ContextMenu cellMenu = new ContextMenu();
                                    final MenuItem emailMenuItem = new MenuItem("change Email color to blue");
                                    emailMenuItem.setOnAction(new EventHandler<ActionEvent>(){
                                        @Override
                                        public void handle(ActionEvent event) {
                                            //set cell style
                                            cell.setStyle(" -fx-background-color: deepskyblue;");
                                            String cellData = cell.getItem();
                                            Person person = (Person) cell.getTableRow().getItem();
                                            System.out.println("Email " + person + " at " + cellData);
                                        }
                                    });
                                    cellMenu.getItems().add(emailMenuItem);
                                    cell.setContextMenu(cellMenu);
                                } else {
                                    cell.setContextMenu(null);
                                }
                            }
                        });
                        return cell;
                    }
                });

        table.setItems(data);
        table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);

        final TextField addFirstName = new TextField();
        addFirstName.setPromptText("First Name");
        addFirstName.setMaxWidth(firstNameCol.getPrefWidth());
        final TextField addLastName = new TextField();
        addLastName.setMaxWidth(lastNameCol.getPrefWidth());
        addLastName.setPromptText("Last Name");
        final TextField addEmail = new TextField();
        addEmail.setMaxWidth(emailCol.getPrefWidth());
        addEmail.setPromptText("Email");

        final Button addButton = new Button("Add");

        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                data.add(new Person(
                        addFirstName.getText(),
                        addLastName.getText(),
                        addEmail.getText()));
                addFirstName.clear();
                addLastName.clear();
                addEmail.clear();
            }
        });

        hb.getChildren().addAll(addFirstName, addLastName, addEmail, addButton);
        hb.setSpacing(3);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table, hb);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
    }

    public static class Person {

        private final SimpleStringProperty firstName;
        private final SimpleStringProperty lastName;
        private final SimpleStringProperty email;

        private Person(String fName, String lName, String email) {
            this.firstName = new SimpleStringProperty(fName);
            this.lastName = new SimpleStringProperty(lName);
            this.email = new SimpleStringProperty(email);
        }

        public String getFirstName() {
            return firstName.get();
        }

        public void setFirstName(String fName) {
            firstName.set(fName);
        }

        public String getLastName() {
            return lastName.get();
        }

        public void setLastName(String fName) {
            lastName.set(fName);
        }

        public String getEmail() {
            return email.get();
        }

        public void setEmail(String fName) {
            email.set(fName);
        }
    }
}

